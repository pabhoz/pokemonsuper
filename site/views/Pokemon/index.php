<html>
<head>
    <title><?php echo $this->title; ?></title>
    <?php $this->loadModule("scripts"); ?>
</head>
<body>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
echo "<br><br><br><br>";
?>
    <h1>Usuarios registrados:</h1>
    <ul>
    <?php foreach ($this->users as $key => $user): ?>
    <li><?php print($user["username"]); ?></li>
    <?php endforeach; ?>
    </ul>
    
    <h1>Nuevo Usuario:</h1>
    <form>
        <input type="text" name="username" placeholder="username">
        <input type="password" name="password" placeholder="password">
        <input type="email" name="email" placeholder="email">
        <input type="submit">
    </form>
    
    <script>
        $(function(){
           $("form").submit(function(e){
               e.preventDefault();
               console.log({
                      username:$("input[name='username']").val(),
                      password:$("input[name='password']").val(),
                      email:$("input[name='email']").val()
                    });
                    
               $.ajax({
                  url: "<?php print(URL); ?>User/signup",
                  method: "POST",
                  data: {
                      username:$("input[name='username']").val(),
                      password:$("input[name='password']").val(),
                      email:$("input[name='email']").val()
                    }
               }).done(function(response){
                   console.log(response);
               });
           }) 
        });
    </script>
    
    
</body>
</html>