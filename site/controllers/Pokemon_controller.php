<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class Pokemon_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        $this->view->users = User::getAll();
        $this->view->render($this,"index","Pokemón");
    }
    
}
