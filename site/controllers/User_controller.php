<?php
/**
 * Description of Index_controller
 *
 * @author pabhoz
 */

class User_controller extends \Fox\FoxController{
   
    function __construct() {
        parent::__construct();
    }

    public function index()
    {
        //$this->view->debug = true;
        $this->view->render($this,"index","View Title");
    }
    
    public function signup(){
        $data = filter_input_array(INPUT_POST);
        $usr = new User(null,$data["username"],$data["password"],$data["email"]);
        $r = $usr->create();
        echo json_encode($r);
    }
    
}
